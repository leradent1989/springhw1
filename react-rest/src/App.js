import React, {Component} from 'react';

import './App.css';
import axios from 'axios';
import Card from './Card';
import account from "./Account";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {message:[],
    accounts:[]};
  }

  componentDidMount() {
    const url = 'http://localhost:9000/customers';
    axios.get(url).then(response => {
      console.log('response : ');
      console.log(response);
      this.setState((current) => {

        const newState = { ...current }
        newState.message= response.data
        return newState
      }
    )}).catch(error => {
      console.log(error);
    });
    axios.get('http://localhost:9000/accounts').then(response => {
      console.log('response : ');
      console.log(response);
      this.setState((current) => {

            const newState = { ...current }
            newState.accounts= response.data
            return newState
          }
      )}).catch(error => {
      console.log(error);
    });


      fetch(url).then(response => response.json()).then(result =>{

      let customers = result

        fetch('http://localhost:9000/accounts').then(response => response.json()).then(result =>{
          console.log(customers)
          console.log(result)

          let customerAccounts = [];
          customers.forEach(customer =>{
            customer.accounts.forEach(account =>{

                customerAccounts.push(account)

            })
          })
          fetch( `http://localhost:9000/accounts/customerAccounts`, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify(customerAccounts)})




        })

      })

}


  render() {
    const {message,accounts} = this.state;


    console.log(message)
    console.log(accounts)
    return(
        <>
<div className="App">
          {message.map(({id,name,email,age,accounts}) =><Card key={id}  id ={id}  name= {name} email ={email} age ={age} accounts={accounts} ></Card>)}
  </div>
        </>)
  }
}

export default App;