package com.danit.springrest.model.dao;

import java.util.List;

public interface AccountDao <Account> {
    Account  save(Account account );

    boolean delete(Account account );
    void deleteAll(List <Account> accountList );
    void saveAll (List <Account > accountList );
    List<Account > findAll();
    boolean deleteById(long id);
    Account  getOne(long id);

}
