package com.danit.springrest.model.service;

import com.danit.springrest.model.Account;
import com.danit.springrest.model.Customer;

import java.util.List;

public interface AccountService {
    Account save(Account account );

    boolean delete(Account account );
    void deleteAll(List<Account> accountList );
    void saveAll (List <Account > accountList );
    List<Account > findAll();
    boolean deleteById(long id);
    Account  getOne(long id);
    void addCustomer(Long accountId,Customer customer);
}
