package com.danit.springrest.model.service;

import com.danit.springrest.model.Account;
import com.danit.springrest.model.Customer;
import com.danit.springrest.model.dao.AccountDao;
import com.danit.springrest.model.dao.MockAccountDao;
import com.danit.springrest.model.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class DefaultAccountService implements AccountService {
    private MockAccountDao accountDao;

@Autowired
    public DefaultAccountService(MockAccountDao accountDao) {
        this.accountDao = accountDao;
    }


    public Account save(Account account) {


        return accountDao.save(account);
    }

    public boolean delete(Account account) {

        return accountDao.delete(account);
    }


    public void deleteAll(List<Account> accountList) {
        accountDao.deleteAll(accountList);
    }

    public List<Account> findAll() {
        return accountDao.findAll();
    }

    public void saveAll(List<Account> accountList) {
        accountDao.saveAll(accountList);
    }

    public boolean deleteById(long id) {
        return accountDao.deleteById(id);
    }

    public Account getOne(long id) {

        return accountDao.getOne(id);


    }
    public void addCustomer(Long accountId,Customer customer){
    getOne(accountId ).setCustomer(customer);

    }
}