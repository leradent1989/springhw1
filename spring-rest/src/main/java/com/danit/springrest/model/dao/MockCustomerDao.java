package com.danit.springrest.model.dao;

import com.danit.springrest.model.Account;
import com.danit.springrest.model.Currency;
import com.danit.springrest.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

@Repository
@Lazy
public class MockCustomerDao implements CustomerDao {
    private List<Customer> customers = new ArrayList<>();
@Autowired
    public MockCustomerDao(List<Customer> customers ) {
        this.customers  = customers ;
    }
    @PostConstruct
    public void init() {
         customers.add(  new Customer("Alex Smith", "gtrgtg@gmail.com", 23) ) ;
         customers.add( new Customer("Cris Thomson", "feers2@gmail.com", 34));
         customers.add( new Customer("Roger Williams","dtrgrvrg@gmail.com",43));
         customers.add( new Customer("Thomas Spencer","dtrgravrg@gmail.com",35));


        AtomicReference<Long> count = new AtomicReference<>(1L);
        AtomicReference<Long> count1 = new AtomicReference<>(6L);
        AtomicReference<Long> count2 = new AtomicReference<>(1L);
        customers.stream().forEach(customer ->{

            customer.setId(count.getAndSet(count.get() + 1));
            List <Account> accounts = new ArrayList<>();
            Account   account1 =new Account(Currency.EUR,new Customer(customer.getName(), customer.getEmail() , customer.getAge()) );
            account1.setId(count2.getAndSet(count2.get() + 1));
            accounts.add(account1 );

            customer.setAccounts(accounts);


        });

        customers.stream().forEach(customer ->{


            List <Account> accounts =customer.getAccounts();
            Account   account1 =new Account(Currency.UAH,new Customer(customer.getName(), customer.getEmail() , customer.getAge()) );
            account1.setId(count1.getAndSet(count1.get() + 1));
            accounts.add(account1 );

            customer.setAccounts(accounts);


        });

    }

    public  Customer   save(Customer  customer ){
        customer .setId(Long.parseLong(String.valueOf(customers .size()  + 1)));
        customers .add(customer );
      return customer ;
    }

    public   boolean delete(Customer  customer ){
        customers .remove(customer) ;
        if(customers .contains(customer )){
            return false;
        }
        return true ;
    }


    public   void deleteAll(List <Customer > customers ){
        customers .clear();
    }

    public   List<Customer  > findAll(){
        return customers  ;
    }
    public   void saveAll (List <Customer  > customers ) {
        this.customers  = customers ;
    }

    public    boolean deleteById(long id){
        Optional  <Customer> customerOptional = customers .stream().filter(el-> el.getId().equals(id)).findAny();
        if(customerOptional.isEmpty()){
            return false;
        }else{
            customers .remove(customerOptional.get());
            return true;

        }

    }
    public   Customer  getOne(long id){

        Optional  <Customer> customerOptional = customers.stream().filter(el-> el.getId().equals(id)).findAny();
        if(customerOptional.isEmpty()){
            return null;
        }else{

            return  customerOptional.get();

        }


    }



}
