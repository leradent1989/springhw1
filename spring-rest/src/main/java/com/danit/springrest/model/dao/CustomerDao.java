package com.danit.springrest.model.dao;



import com.danit.springrest.model.Customer;

import java.util.List;

public interface CustomerDao  {
    Customer save(Customer customer );

    boolean delete(Customer customer);
    void deleteAll(List<Customer> customers);
    void saveAll(List<Customer> customers);
    List<Customer> findAll();
    boolean deleteById(long id);
    Customer getOne(long id);
}
