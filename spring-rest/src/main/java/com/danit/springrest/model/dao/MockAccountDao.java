package com.danit.springrest.model.dao;

import com.danit.springrest.model.Account;
import com.danit.springrest.model.Currency;
import com.danit.springrest.model.Customer;
import com.danit.springrest.model.dao.AccountDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;

@Repository
@Lazy
public class MockAccountDao implements AccountDao<Account> {
    private List<Account> accountList  = new ArrayList<>();
     @Autowired
    public MockAccountDao(List<Account> accountList) {
        this.accountList = accountList;
    }

    public   List<Account > findAll(){
        return accountList ;
    }


    public  Account  save(Account account ){
    if(accountList.size() == 8){      account.setId(Long.parseLong(String.valueOf(accountList.size()  + 2))); }else{
        account.setId(Long.parseLong(String.valueOf(accountList.size()  + 1)));}
       try{

           if(UUID.fromString(account.getNumber()).getClass() == UUID.class ){
             account .setNumber(account.getNumber());
           }}finally {
           accountList.add(account);

           return account;
       }

   }

  public   boolean delete(Account account ){
        accountList.remove(account);
        if(accountList.contains(account)){
            return false;
        }
        return true ;
  }


    public   void deleteAll(List <Account > accountList ){
      accountList.clear();
  }


      public   void saveAll (List <Account > newAccountList ) {
          this.accountList = newAccountList;

      }
      public  boolean deleteById(long id){
     Optional  <Account> accountOptional = accountList.stream().filter(el-> el.getId().equals(id)).findAny();
     if(accountOptional.isEmpty()){
         return false;
     }else{

         accountList.remove(accountOptional.get());
         return true;

     }

 }
      public   Account  getOne(long id){

      Optional  <Account> accountOptional = accountList.stream().filter(el-> el.getId().equals(id)).findAny();
      if(accountOptional.isEmpty()){
          return null;
      }else{

          return  accountOptional.get();

      }

  }


}
