package com.danit.springrest.resources;

import com.danit.springrest.model.Account;
import com.danit.springrest.model.Customer;
import com.danit.springrest.model.service.AccountService;
import com.danit.springrest.model.service.CustomerService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.List;

   @RestController
   @RequestMapping("/customers")
   @CrossOrigin(origins = {"http://localhost:3000"})
   public class CustomerRestController {
      private CustomerService customerService;
      private AccountService accountService;

      public CustomerRestController(CustomerService customerService, AccountService accountService) {
        this.customerService  = customerService;
        this.accountService  = accountService;
    }


    @GetMapping

    public List <Customer> getAll(){
        return customerService.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity <?> getById(@PathVariable Long id){
        try{
        return  ResponseEntity.ok(customerService.getOne(id));}
        catch (Exception e){
            return ResponseEntity.badRequest().body("Customer not found");
        }
    }

    @PostMapping

    public  ResponseEntity <?>  addCustomer(@RequestBody Customer customer ){
        try{

        return  ResponseEntity.ok(   customerService.save(new Customer(customer.getName(), customer.getEmail(),customer.getAge())));


        }catch (Exception  e){
            return ResponseEntity.badRequest().body("Cannot add customer");
        }

    }

    @DeleteMapping("/{id}")

    public ResponseEntity <?>  deleteById(@PathVariable  Long id){
   try{
       customerService.deleteById(id);
       return ResponseEntity.ok().build();

   }

   catch(Exception e){

     return  ResponseEntity.badRequest().body("Cannot delete customer");

   }
    }
    @DeleteMapping

    public boolean deleteCustomer(@RequestBody Customer customer ){

        Customer deletedCustomer = customerService.getOne(customer.getId());

          return customerService .delete(deletedCustomer);
    }
      @PutMapping

    public ResponseEntity <?> editCustomer(@RequestBody Customer customer){

        try{

        Customer editedCustomer = customerService.getOne(customer.getId());
        editedCustomer.setName(customer.getName());
        editedCustomer .setAge(customer.getAge());
        editedCustomer.setEmail(customer.getEmail());
        editedCustomer.getAccounts().forEach(account -> {account.setCustomer(new Customer(editedCustomer.getName(),editedCustomer.getEmail(),editedCustomer.getAge()));});

        return    ResponseEntity.ok(editedCustomer );
    }
        catch (Exception e){

  return   ResponseEntity.badRequest().body("Cannot update customer");
    }

  }

      @PutMapping("/account/{id}")

        public ResponseEntity <?> addAccount(@PathVariable Long id,@RequestBody Account account ){
        try{
            if(accountService.getOne(id) == null ){
                return ResponseEntity.badRequest().body("Account you are trying to add does not exist");
            }
             Customer customer = customerService.getOne(id);
            Account account1 =accountService.getOne(account.getId());
            account1.setCustomer(new Customer(customer.getName(),customer.getEmail(),customer.getAge()));

              List <Account > customerAccounts = customer.getAccounts();

          if(customerAccounts.contains(accountService.getOne(account.getId() ))){
         return ResponseEntity.badRequest().body("Account already in list");
     }

     customerAccounts.add(account1);
     customer.setAccounts(customerAccounts);

          return  ResponseEntity.ok( customer);

        }catch (Exception e){
           return ResponseEntity.badRequest().body("Cannot update customer");
        }

}
    @DeleteMapping("/account/{id}")

         public ResponseEntity <?> deleteAccount(@PathVariable  Long id, @RequestBody Account account ){
        try{
            if(accountService.getOne(id) == null){
                return ResponseEntity.badRequest().body("Account does not exist");
            }
        Customer customer = customerService.getOne(id);

        List <Account > customerAccounts = customer.getAccounts();

            if(!customerAccounts.contains(accountService.getOne(account.getId()))){
                return ResponseEntity.badRequest().body("This account isn't in the list ");
            }

        customerAccounts.remove(accountService.getOne(account.getId()));

        customer.setAccounts(customerAccounts);

        return ResponseEntity.ok(customer)  ;
        }
        catch (Exception e){

            return ResponseEntity.badRequest().body("Can not update customer");
}

    }

}
