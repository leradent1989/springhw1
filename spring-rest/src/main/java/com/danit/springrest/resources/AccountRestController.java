package com.danit.springrest.resources;

import com.danit.springrest.model.Account;
import com.danit.springrest.model.Currency;
import com.danit.springrest.model.Customer;
import com.danit.springrest.model.service.AccountService;
import com.danit.springrest.model.service.CustomerService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/accounts")
@CrossOrigin(origins = {"http://localhost:3000"})

public class AccountRestController {
    private AccountService accountService;
    private CustomerService customerService;

    public AccountRestController(AccountService accountService,CustomerService customerService) {
        this.accountService = accountService;
        this.customerService =customerService;
    }

    @GetMapping

    public ResponseEntity <?>  getAll(){

       try{
        return  ResponseEntity.ok(accountService.findAll());}
       catch (Exception e){
         return   ResponseEntity.badRequest().body("Can not find accounts");
       }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id){
        try{
            return ResponseEntity.ok(accountService.getOne(id));

        }
        catch (Exception e){

            return ResponseEntity.badRequest().body("Account not found");
        }

    }

    @PutMapping("/balance")
    public ResponseEntity <?>  addMoney(  @RequestParam UUID  number, @RequestParam Double  amount ){
     Optional   <Account> accountOptional = accountService.findAll().stream().filter(el->el.getNumber().equals(number.toString())).findAny() ;

        if(accountOptional.isEmpty()){
            return ResponseEntity.badRequest().body("Account not found");
        }
       Account editedAccount = accountOptional.get();

        Double accountBalance =  accountOptional.get().getBalance();

        Double accountNewBalance = accountBalance + amount ;

        editedAccount.setBalance(accountNewBalance);
        customerService.findAll().forEach(customer -> {
            customer.getAccounts().forEach(account -> {
                if(account.getId().equals(editedAccount.getId())){
                    account.setBalance(accountNewBalance);
                }
            });
        });


        return ResponseEntity.ok().build();

    }
    @DeleteMapping("/balance")
    public ResponseEntity <?>  takeMoney(  @RequestParam  UUID  number, @RequestParam Double  amount ){
        Optional  <Account> accountOptional = accountService.findAll().stream().filter(el->el.getNumber().equals(number.toString())).findAny();
        if(accountOptional.isEmpty()){
            return ResponseEntity.badRequest().body("Account not found");
        }
        Account editedAccount = accountOptional.get();

        Double accountBalance =  accountOptional.get().getBalance();
        if(accountBalance - amount< 0){
            return ResponseEntity.badRequest().body("There is not enough money on your account");
        }

        Double accountNewBalance = accountBalance - amount;

        editedAccount.setBalance(accountNewBalance);
        customerService.findAll().forEach(customer -> {
            customer.getAccounts().forEach(account -> {
                if(account.getId().equals(editedAccount.getId())){
                    account.setBalance(accountNewBalance);
                }
            });
        });

        return ResponseEntity.ok().build();

    }

    @PutMapping("/balance/transaction")

    public ResponseEntity <?>  transactionMoney(  @RequestParam  UUID  numberAccountFrom, @RequestParam  UUID  numberAccountTo, @RequestParam Double  amount ){
        Optional  <Account> accountFromOptional = accountService.findAll().stream().filter(el->el.getNumber().equals(numberAccountFrom.toString())).findAny();
        if(accountFromOptional.isEmpty()){
            return ResponseEntity.badRequest().body("Account not found");
        }
        Optional  <Account> accountToOptional = accountService.findAll().stream().filter(el->el.getNumber().equals(numberAccountTo.toString())).findAny();
        if(accountToOptional.isEmpty()){
            return ResponseEntity.badRequest().body("Account not found");
        }
      if(accountFromOptional.get().getBalance() -amount <0){

          return ResponseEntity.badRequest().body("Not enough money on account");
      }
        takeMoney(numberAccountFrom ,amount);
        addMoney(numberAccountTo,amount) ;

        return ResponseEntity.ok().build();

    }
    @PostMapping

    public  ResponseEntity <?>  addAccount(@RequestBody Account account ){
        if(accountService.findAll().contains(accountService.getOne(account.getId()))){
            return ResponseEntity.badRequest().body("Account already exists");
        }
     Account newAccount = new Account(account.getCurrency(),account.getCustomer());

    try{
        if(UUID.fromString(account.getNumber()).getClass() == UUID.class ){
              newAccount.setNumber(account.getNumber());
       }}finally {

           return ResponseEntity.ok(accountService.save(newAccount));
      }

    }

    @DeleteMapping("/{id}")

    public ResponseEntity <?> deleteById(@PathVariable Long id){
      try{
            customerService.findAll().forEach(customer -> {
            if(customer.getAccounts().contains(accountService.getOne(id))){
            List <Account> customerAccounts =    customer.getAccounts();
              customerAccounts .remove(accountService.getOne(id));
              customer.setAccounts(customerAccounts);
      }
  });
          accountService.deleteById(id);
               return ResponseEntity.ok().build() ;

      }catch (Exception e){
       return ResponseEntity.badRequest().body("");
      }
    }
    @DeleteMapping

    public ResponseEntity <?> deleteAccount(@RequestBody Account account){
   try{
       accountService.delete(accountService.getOne(account.getId()));

       return  ResponseEntity.ok().build();

    }catch (Exception e){

        return ResponseEntity.badRequest().body("Cannot delete account");

}

    }
    @PostMapping("/customerAccounts")

    public ResponseEntity<?> addCustomerAccounts(@RequestBody List<Account > customerAccounts){
        accountService.saveAll(customerAccounts);

       return ResponseEntity.ok().build() ;

    }


}
